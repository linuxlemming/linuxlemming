# Introduction

### Who am I?
* Long time Linux Desktop user and new to the server realm.
* Hobbyist who knows enough to start projects but doesn't have enough free time to maintain or spend hours troubleshooting. 
* A "lemming" who appreciates concise, up to date documentation and wants to see more of it widely available.
* Avid podcast listener.


### What Will This Project Be Like?
* Place for people to listen/read to my journey via podcast and blogs. Will pick projects and blindly follow the documentation and report back on successes and roadblocks.
    - Will submit upstream changes to projects on documentation if possible.
    - Will submit tutorial if appropriate to other projects.
* This will not be a dedicated information hub!
    - So many already exist, would rather contribute than reinvent the wheel.
* Community contributions are WANTED!
    - Want to try your hand at podcasting? Do it here!!
    - Want to submit your experience with a project/documentation, do it here!
    - Try to keep everything within GitLab for easy additions.

### Is there a schedule?
* Not really. . . 
    - Can only record audio at night or on days when alone due to high volumes of ambient noise.
    - Will try for 1 new addition per week or biweekly as time permits.
    - If community involvement picks up, then more will be released.
* Currently not on iTunes/PocketCasts/Spotify/Sticher/etc.
    - Don't really plan on doing these unless I have time or community involvement picks up.

### First "Official" Episode?
* I wouldn't consider this an official submission based on the ideals of the project. This is more of an obligatory episode, thus the E00 tag.
* Haven't settled on the topic for E01 yet, maybe how to use Git since I have documentation for that already and that is how I would prefer contributions to roll in.